# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.ClientTest.OverrideEndPoints do
  @moduledoc """
  Protocol that indicates that the test sever overrides the handling of endpoints
  """

  @doc """
  Process the connection and optionally responds.

  This function should (ignoring the `server` parameter) be a function plug as
  define by `Plug`, and follow the conventions from `Plug.Builder`.  In
  particular, if it handles the request, it should return
  `Plug.Conn.halt(conn)`.

  """
  @spec call(Polyjuice.ClientTest.OverrideEndPoints.t(), Plug.Conn.t(), Keyword.t()) ::
          Plug.Conn.t()
  def call(server, conn, opts)
end
