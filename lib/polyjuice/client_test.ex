# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest do
  use Application

  def start(_type, _args) do
    children = [
      %{
        id: Polyjuice.ClientTest.DashboardServer,
        start:
          {GenServer, :start_link,
           [
             Polyjuice.ClientTest.DashboardServer,
             Application.get_env(:polyjuice_client_test, :server_options, [
               "test.localhost",
               ["Polyjuice.ClientTest.Test.*"]
             ]),
             [name: Polyjuice.ClientTest.DashboardServer]
           ]}
      },
      {Plug.Cowboy,
       scheme: Application.get_env(:polyjuice_client_test, :cowboy_scheme, :http),
       plug: Polyjuice.ClientTest.Routes,
       options: Application.get_env(:polyjuice_client_test, :cowboy_options, ip: {127, 0, 0, 1})}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
