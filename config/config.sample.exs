use Mix.Config

config :polyjuice_client_test,
  # Test server options.  The first element is the host name for the dashboard.
  # The second element is a list of globs indicating which modules to use for
  # tests.  After that is a keyword list of options.  Currently, the only
  # option is `test_domain`, which is the base domain that tests will run on.
  # For example, if `test_domain` is `"test.example.org"`, then tests will use
  # the server name `<test_id>.test.example.org`.  If the test domain is not
  # specified, then it defaults to being the same as the dashboard host name.
  # If not set, defaults to using `test.localhost` as the host name, and getting
  # tests from `Polyjuice.ClientTest.Test.*`.
  server_options: [
    "dashboard.example.org",
    ["Polyjuice.ClientTest.Test.*"],
    test_domain: "tests.example.org"
  ],
  # The scheme to use.  Either `:http` or `:https`.  If using `:https`, the
  # server must be behind a reverse proxy that handles HTTPS.  If not set,
  # defaults to `:http`.
  cowboy_scheme: :http,
  # Options for the Cowboy web server.  See
  # https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html for the available options.
  # If not set, defaults to only listening on 127.0.0.1:4000
  cowboy_options: [ip: {127, 0, 0, 1}]
