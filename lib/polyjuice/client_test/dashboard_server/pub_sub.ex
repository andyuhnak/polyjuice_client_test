# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.DashboardServer.PubSub do
  @moduledoc false
  @behaviour Polyjuice.Server.Protocols.Helper.PubSub

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def publish({data, queues}, _, state) do
    Enum.each(queues, fn queue ->
      state.subscribers
      |> Map.get(queue, [])
      |> Enum.each(&Polyjuice.Server.Protocols.Subscriber.notify(&1, queue, data))
    end)

    {:reply, :ok, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def subscribe({subscriber, queues}, _, state) do
    new_subscribers =
      Enum.reduce(queues, state.subscribers, fn queue, subscribers ->
        Map.get(subscribers, queue, MapSet.new())
        |> MapSet.put(subscriber)
        |> (&Map.put(subscribers, queue, &1)).()
      end)

    {:reply, :ok, %{state | subscribers: new_subscribers}}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def unsubscribe({subscriber, queues}, _, state) do
    new_subscribers =
      Enum.reduce(queues, state.subscribers, fn queue, subscribers ->
        subscriber_set =
          Map.get(subscribers, queue, MapSet.new())
          |> MapSet.delete(subscriber)

        if MapSet.size(subscriber_set) == 0 do
          Map.delete(subscribers, queue)
        else
          Map.put(subscribers, queue, subscriber_set)
        end
      end)

    {:reply, :ok, %{state | subscribers: new_subscribers}}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def await_sync({user_id, device_id, sync_token, timeout, filter}, from, state) do
    Task.start(fn ->
      Polyjuice.Server.SyncHelper.sync(
        state.sync_helper,
        state.server,
        user_id,
        device_id,
        sync_token,
        timeout,
        filter
      )
      |> (&GenServer.reply(from, &1)).()
    end)

    {:noreply, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def get_sync_data_since({user_id, device_id, sync_token, filter}, _, state) do
    # FIXME:
    {:reply, {nil, %{}}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def add_filter({user_id, filter}, _from, state) do
    # FIXME: this is a stub
    {:reply, "", state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def get_filter({user_id, filter_id}, _from, state) do
    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Not found",
        plug_status: 404,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
      }}, state}
  end
end
