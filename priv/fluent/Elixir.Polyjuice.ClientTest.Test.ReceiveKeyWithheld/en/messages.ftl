check-blocked = In the "Key withheld test" room, there should be two messages: one that
  is undecryptable (with an indication that this is due to the recipient being blocked),
  and one that is decryptable.  Is this what you see?
check-unverified = Now there should be two more messages: one that is undecryptable
  (with an indication that this is due to keys not being sent to unverified
  recipients), and one that is decryptable.  Is this what you see?
