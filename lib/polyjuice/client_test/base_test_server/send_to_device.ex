# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.SendToDevice do
  @behaviour Polyjuice.Server.Protocols.Helper.SendToDevice

  @type to_device_stream() :: %{
          optional({String.t(), String.t()}) => list({pos_integer, map}) | [{pos_integer, :empty}]
        }
  @type transactions_set() :: %{optional({String.t(), String.t()}) => MapSet.t(String.t())}

  @type required_state() :: %{
          to_device_stream: to_device_stream(),
          to_device_transactions: transactions_set(),
          users: Polyjuice.ClientTest.BaseTestServer.User.user_map()
        }

  @type optional_state_pub_sub() :: %{
          subscribers: %{optional(any) => Polyjuice.Server.Protocols.Subscriber.t()}
        }

  @impl Polyjuice.Server.Protocols.Helper.SendToDevice
  def send_to_device({user_id, device_id, txn_id, event_type, messages}, from, state) do
    # FIXME: drop messages destined for non-local users
    user_transactions = Map.get(state.to_device_transactions, {user_id, device_id}, MapSet.new())

    if MapSet.member?(user_transactions, txn_id) do
      {:reply, :ok, state}
    else
      state =
        put_in(
          state.to_device_transactions[{user_id, device_id}],
          MapSet.put(user_transactions, txn_id)
        )

      state =
        Enum.reduce(messages, state, fn {target_user_id, device_events}, state ->
          Enum.reduce(device_events, state, fn {target_device_id, content}, state ->
            full_event = %{"sender" => user_id, "type" => event_type, "content" => content}

            # queue the event to be sent to a specific device
            send_to = fn target_device_id, state ->
              [{stream_pos, _} | _] =
                device_stream =
                case Map.get(state.to_device_stream, {target_user_id, target_device_id}, []) do
                  [] ->
                    [{1, full_event}]

                  [{pos, :empty}] ->
                    [{pos + 1, full_event}]

                  [{pos, _} | _] = stream ->
                    [{pos + 1, full_event} | stream]
                end

              if Map.has_key?(state, :subscribers) do
                Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
                  {{:to_device, stream_pos, full_event}, [{target_user_id, target_device_id}]},
                  from,
                  state
                )
              end

              state =
                Polyjuice.ClientTest.BaseTestServer.emit(
                  {:send_to_device, target_user_id, target_device_id, full_event},
                  state
                )

              %{
                state
                | to_device_stream:
                    Map.put(
                      state.to_device_stream,
                      {target_user_id, target_device_id},
                      device_stream
                    )
              }
            end

            if target_device_id == "*" do
              {:reply, devices, _} =
                Polyjuice.ClientTest.BaseTestServer.User.get_user_devices(
                  {target_user_id},
                  from,
                  state
                )

              Enum.reduce(devices, state, send_to)
            else
              send_to.(target_device_id, state)
            end
          end)
        end)

      {:reply, :ok, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.SendToDevice
  def mark_received({user_id, device_id, position}, _from, state) do
    state =
      update_in(state.to_device_stream[{user_id, device_id}], fn
        nil ->
          [{position, :empty}]

        [{_, :empty}] = stream ->
          stream

        stream ->
          case Enum.take_while(stream, fn {pos, _event} -> pos > position end) do
            [] -> [{position, :empty}]
            stream -> stream
          end
      end)

    {:reply, :ok, state}
  end
end
