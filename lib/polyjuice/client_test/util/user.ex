# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.User do
  @moduledoc """
  Utility functions for users.

  This module defines a struct that represents a user, and manages data
  associated with the user.  In addition to the functions defined in this
  module, the struct can be used and manipulated by some functions in other
  modules, such as `Polyjuice.ClientTest.Util.Device` or
  `Polyjuice.ClientTest.Util.CrossSigning`.
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.ClientTest.Util

  use TypedStruct

  typedstruct do
    field :server, BaseTestServer.t(), enforce: true
    field :id, String.t(), enforce: true
    field :password, String.t() | :no_login, enforce: true
    field :cross_signing, Util.CrossSigning.t() | nil

    field :ssss, %{keys: %{optional(String.t()) => String.t()}, default: String.t() | nil},
      default: %{keys: %{}, default: nil}

    field :key_backup, Util.KeyBackup.t() | nil
    field :other, map(), default: %{}
  end

  @doc """
  Create a new user struct.
  """
  @spec create(BaseTestServer.t(), String.t(), String.t()) :: __MODULE__.t()
  def create(server, user_id, password) do
    {user_id, password} = BaseTestServer.create_user(server, user_id, password)

    %__MODULE__{
      server: server,
      id: user_id,
      password: password
    }
  end
end
