# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServerTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  test "create user" do
    test_script(fn test_server ->
      {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

      assert user == "@test:faketest.test.localhost"
      assert password == "ThisIsATest"

      [
        {alice, _alice_password},
        {bob, _bob_password},
        {carol, _carol_password},
        {dan, _dan_password},
        {erin, _erin_password},
        {frank, _frank_password}
      ] =
        Enum.map(1..6, fn _ ->
          BaseTestServer.create_user(test_server, :aliceandbob, "ThisIsATest")
        end)

      assert alice == "@alice:faketest.test.localhost"
      assert bob == "@bob:faketest.test.localhost"
      assert carol == "@carol:faketest.test.localhost"
      assert dan == "@dan:faketest.test.localhost"
      assert erin == "@erin:faketest.test.localhost"
      assert frank == "@frank:faketest.test.localhost"

      users =
        BaseTestServer.get_and_update_server_state(test_server, fn state ->
          {state.users, state}
        end)

      assert users == %{
               "@test:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@alice:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@bob:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@carol:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@dan:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@erin:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"},
               "@frank:faketest.test.localhost" => %{devices: %{}, password: "ThisIsATest"}
             }
    end)
  end

  test "wait for event" do
    test_script(fn test_server ->
      {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

      Task.start(fn ->
        Process.sleep(200)
        Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])
      end)

      true = BaseTestServer.wait_until_event(test_server, &match?({:login, _, _, _}, &1), 1000)
    end)
  end

  test "wait for state change" do
    test_script(
      fn test_server ->
        {user, password} = BaseTestServer.create_user(test_server, "test", "ThisisATest")

        {:ok, %{device_id: device_id}} =
          Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])

        true = BaseTestServer.wait_until_state(test_server, &Map.get(&1, :logged_in), 2000)

        Task.start(fn ->
          Process.sleep(200)
          Polyjuice.Server.Protocols.User.log_out(test_server, user, device_id)
        end)

        true =
          BaseTestServer.wait_until_state(test_server, &(Map.get(&1, :logged_in) == false), 2000)
      end,
      fn
        {:login, _, _, _}, state, _ -> Map.put(state, :logged_in, true)
        {:logout, _, _}, state, _ -> Map.put(state, :logged_in, false)
        _, state, _ -> state
      end
    )
  end
end
