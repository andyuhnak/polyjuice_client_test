# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.BaseClientServer do
  @behaviour Polyjuice.Server.Protocols.Helper.BaseClientServer

  @type required_state :: %{
          server: %{servername: String.t()},
          access_tokens: %{optional(String.t()) => %{user_id: String.t(), device_id: String.t()}}
        }

  @impl Polyjuice.Server.Protocols.Helper.BaseClientServer
  def get_server_name({}, _from, state) do
    {:reply, state.server.servername, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.BaseClientServer
  def user_from_access_token({access_token}, _from, state) do
    ret =
      case Map.get(state.access_tokens, access_token) do
        %{user_id: user_id, device_id: device_id} -> {:user, user_id, device_id, false}
        _ -> nil
      end

    {:reply, ret, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.BaseClientServer
  def user_in_appservice_namespace({_, _, _}, _from, state) do
    {:reply, false, state}
  end
end
