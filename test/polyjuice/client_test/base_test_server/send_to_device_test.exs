# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.SendToDeviceTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  test "send to device" do
    test_script(
      fn server ->
        {user, password} = BaseTestServer.create_user(server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id1, access_token: access_token1}} =
          Polyjuice.Server.Protocols.User.log_in(server, {:password, user, password}, [])

        {:ok, %{device_id: device_id2, access_token: access_token2}} =
          Polyjuice.Server.Protocols.User.log_in(server, {:password, user, password}, [])

        sync_task =
          Task.async(fn ->
            Polyjuice.Server.Protocols.PubSub.await_sync(
              server,
              user,
              device_id2,
              nil,
              30000,
              %{}
            )
          end)

        # wait for the sync helper to start
        Process.sleep(100)

        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          user,
          device_id1,
          "txn1",
          "org.example.1",
          %{user => %{device_id2 => %{}}}
        )

        {sync_token, sync_data} = Task.await(sync_task)

        sync_data = Map.drop(sync_data, ["device_one_time_keys_count"])

        assert sync_data == %{
                 "to_device" => %{
                   "events" => [%{"sender" => user, "type" => "org.example.1", "content" => %{}}]
                 }
               }

        # make sure we get the same result when going back to the beginning of
        # the stream
        assert Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
                 server,
                 user,
                 device_id2,
                 nil,
                 %{}
               ) == {sync_token, sync_data}

        Polyjuice.Server.Protocols.PubSub.await_sync(
          server,
          user,
          device_id2,
          sync_token,
          0,
          %{}
        )

        # since we called sync with the next token, we shouldn't get the old
        # to-device event when starting from the beginning
        assert Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
                 server,
                 user,
                 device_id2,
                 nil,
                 %{}
               ) == {sync_token, %{}}

        # test that messages are received in the right order, and that messages
        # to "*" work
        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          user,
          device_id1,
          "txn2",
          "org.example.2",
          %{user => %{device_id2 => %{}}}
        )

        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          user,
          device_id1,
          "txn3",
          "org.example.3",
          %{user => %{"*" => %{}}}
        )

        {sync_token2, sync_data} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            user,
            device_id2,
            nil,
            %{}
          )

        assert sync_data == %{
                 "to_device" => %{
                   "events" => [
                     %{"sender" => user, "type" => "org.example.2", "content" => %{}},
                     %{"sender" => user, "type" => "org.example.3", "content" => %{}}
                   ]
                 }
               }

        assert Polyjuice.Server.SyncToken.get_component(sync_token2, :d) >
                 Polyjuice.Server.SyncToken.get_component(sync_token, :d)

        # We don't know the order of devices that the last message will be sent
        # to, so compare sorted versions
        {events2, events1} =
          BaseTestServer.get_test_state(server, &Map.get(&1, :events, [])) |> Enum.split(2)

        assert events1 == [
                 {:send_to_device, user, device_id2,
                  %{"sender" => user, "type" => "org.example.2", "content" => %{}}},
                 {:send_to_device, user, device_id2,
                  %{"sender" => user, "type" => "org.example.1", "content" => %{}}},
                 {:login, user, device_id2, access_token2},
                 {:login, user, device_id1, access_token1}
               ]

        assert Enum.sort(events2) ==
                 Enum.sort([
                   {:send_to_device, user, device_id2,
                    %{"sender" => user, "type" => "org.example.3", "content" => %{}}},
                   {:send_to_device, user, device_id1,
                    %{"sender" => user, "type" => "org.example.3", "content" => %{}}}
                 ])
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end
end
