# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.DeviceKeyTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  test "set and get device keys" do
    test_script(
      fn test_server ->
        {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])

        {pubkey, privkey} =
          {<<71, 147, 206, 109, 245, 113, 97, 55, 178, 43, 26, 108, 229, 171, 63, 255, 34, 207,
             25, 248, 87, 186, 60, 92, 190, 159, 181, 171, 221, 171, 155, 10>>,
           <<156, 1, 81, 214, 0, 61, 60, 255, 213, 105, 136, 90, 39, 0, 100, 85, 145, 159, 186,
             15, 201, 88, 249, 35, 56, 156, 174, 210, 170, 94, 10, 128>>}

        signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: privkey, id: device_id}

        {:ok, device_keys} =
          %{
            "algorithms" => [
              "m.olm.v1.curve25519-aes-sha2",
              "m.megolm.v1.aes-sha2"
            ],
            "device_id" => device_id,
            "keys" => %{
              ("ed25519:" <> device_id) => Base.encode64(pubkey, padding: false)
            },
            "user_id" => user
          }
          |> Polyjuice.Util.JSON.sign(user, signing_key)

        sync_task =
          Task.async(fn ->
            Polyjuice.Server.Protocols.PubSub.await_sync(
              test_server,
              user,
              device_id,
              nil,
              2000,
              %{}
            )
          end)

        # wait for the sync helper to start
        Process.sleep(100)

        Polyjuice.Server.Protocols.DeviceKey.set_device_keys(
          test_server,
          user,
          device_id,
          device_keys
        )

        {sync_token, sync_data} = Task.await(sync_task)

        assert sync_data == %{
                 "device_lists" => %{"changed" => ["@test:faketest.test.localhost"]},
                 "device_one_time_keys_count" => %{}
               }

        assert Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
                 test_server,
                 user,
                 device_id,
                 nil,
                 %{}
               ) == {sync_token, Map.delete(sync_data, "device_one_time_keys_count")}

        keys =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(test_server, user, %{user => []}, nil)

        assert keys == %{"device_keys" => %{user => %{device_id => device_keys}}}

        events = BaseTestServer.get_test_state(test_server, &Map.get(&1, :events, []))

        assert events == [
                 {:set_device_keys, user, device_keys},
                 {:login, user, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  test "upload and claim one-time keys" do
    test_script(
      fn test_server ->
        {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])

        assert Polyjuice.Server.Protocols.DeviceKey.key_counts(test_server, user, device_id) == %{
                 one_time_keys: %{},
                 unused_fallback_keys: []
               }

        Polyjuice.Server.Protocols.DeviceKey.add_one_time_keys(test_server, user, device_id, %{
          "curve25519:AAAAAA" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQ"
        })

        Polyjuice.Server.Protocols.DeviceKey.add_one_time_keys(test_server, user, device_id, %{
          "curve25519:AAAAAB" => "bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR"
        })

        assert Polyjuice.Server.Protocols.DeviceKey.key_counts(test_server, user, device_id) == %{
                 one_time_keys: %{"curve25519" => 2},
                 unused_fallback_keys: []
               }

        assert Polyjuice.Server.Protocols.PubSub.await_sync(
                 test_server,
                 user,
                 device_id,
                 nil,
                 0,
                 %{}
               )
               |> elem(1) == %{"device_one_time_keys_count" => %{"curve25519" => 2}}

        keys =
          Polyjuice.Server.Protocols.DeviceKey.claim_key(test_server, %{
            user => %{device_id => "curve25519"}
          })

        assert Polyjuice.Server.Protocols.DeviceKey.key_counts(test_server, user, device_id) == %{
                 one_time_keys: %{"curve25519" => 1},
                 unused_fallback_keys: []
               }

        # claim should always return keys first-in-first-out
        assert keys == %{
                 user => %{
                   device_id => %{
                     "curve25519:AAAAAA" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQ"
                   }
                 }
               }

        events = BaseTestServer.get_test_state(test_server, &Map.get(&1, :events, []))

        assert events == [
                 {:claim_one_time_keys, %{user => %{device_id => "curve25519"}},
                  %{
                    user => %{
                      device_id => %{
                        "curve25519:AAAAAA" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQ"
                      }
                    }
                  }},
                 {:add_one_time_keys, user, device_id,
                  %{"curve25519:AAAAAB" => "bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR"}},
                 {:add_one_time_keys, user, device_id,
                  %{"curve25519:AAAAAA" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQ"}},
                 {:login, user, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  test "upload cross-signing keys and signatures" do
    test_script(
      fn test_server ->
        [{alice, alice_password}, {bob, _bob_password}] =
          Enum.map(1..2, fn _ ->
            BaseTestServer.create_user(test_server, :aliceandbob, "password")
          end)

        {:ok, %{device_id: alice_device_id, access_token: alice_access_token}} =
          Polyjuice.Server.Protocols.User.log_in(
            test_server,
            {:password, alice, alice_password},
            []
          )

        {alice_device_pubkey, alice_device_privkey} =
          {<<71, 147, 206, 109, 245, 113, 97, 55, 178, 43, 26, 108, 229, 171, 63, 255, 34, 207,
             25, 248, 87, 186, 60, 92, 190, 159, 181, 171, 221, 171, 155, 10>>,
           <<156, 1, 81, 214, 0, 61, 60, 255, 213, 105, 136, 90, 39, 0, 100, 85, 145, 159, 186,
             15, 201, 88, 249, 35, 56, 156, 174, 210, 170, 94, 10, 128>>}

        alice_device_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
          key: alice_device_privkey,
          id: alice_device_id
        }

        alice_unsigned_device_keys = %{
          "algorithms" => [
            "m.olm.v1.curve25519-aes-sha2",
            "m.megolm.v1.aes-sha2"
          ],
          "device_id" => alice_device_id,
          "keys" => %{
            ("ed25519:" <> alice_device_id) => Base.encode64(alice_device_pubkey, padding: false)
          },
          "user_id" => alice
        }

        {:ok, alice_self_signed_device_keys} =
          Polyjuice.Util.JSON.sign(alice_unsigned_device_keys, alice, alice_device_signing_key)

        Polyjuice.Server.Protocols.DeviceKey.set_device_keys(
          test_server,
          alice,
          alice_device_id,
          alice_self_signed_device_keys
        )

        {alice_msk_pubkey, alice_msk_privkey} =
          {<<221, 137, 247, 225, 110, 24, 239, 156, 108, 158, 51, 117, 193, 68, 107, 13, 168, 139,
             191, 181, 2, 194, 194, 149, 10, 37, 247, 182, 214, 7, 87, 71>>,
           <<165, 215, 182, 183, 110, 94, 49, 124, 237, 145, 69, 220, 48, 23, 148, 170, 16, 98,
             54, 38, 69, 166, 95, 112, 124, 16, 26, 176, 12, 159, 191, 36>>}

        alice_msk_b64 = Base.encode64(alice_msk_pubkey, padding: false)

        alice_msk_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
          key: alice_msk_privkey,
          id: alice_msk_b64
        }

        {alice_usk_pubkey, alice_usk_privkey} =
          {<<151, 167, 21, 94, 6, 210, 12, 222, 128, 107, 190, 70, 227, 25, 20, 187, 114, 140,
             119, 21, 71, 93, 233, 135, 241, 102, 170, 88, 222, 137, 99, 156>>,
           <<109, 18, 194, 252, 107, 132, 90, 172, 164, 81, 77, 15, 51, 230, 217, 221, 237, 123,
             139, 64, 212, 40, 202, 63, 247, 151, 181, 158, 192, 68, 38, 242>>}

        alice_usk_b64 = Base.encode64(alice_usk_pubkey, padding: false)

        alice_usk_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
          key: alice_usk_privkey,
          id: alice_usk_b64
        }

        {alice_ssk_pubkey, alice_ssk_privkey} =
          {<<175, 245, 41, 32, 148, 72, 248, 60, 209, 178, 75, 153, 30, 94, 40, 214, 238, 176, 30,
             25, 245, 135, 182, 146, 242, 205, 8, 27, 51, 209, 115, 124>>,
           <<223, 85, 128, 97, 252, 137, 208, 213, 45, 182, 1, 79, 58, 98, 195, 157, 252, 77, 131,
             11, 177, 46, 216, 101, 117, 141, 193, 81, 245, 248, 157, 33>>}

        alice_ssk_b64 = Base.encode64(alice_ssk_pubkey, padding: false)

        alice_ssk_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
          key: alice_ssk_privkey,
          id: alice_ssk_b64
        }

        alice_msk = %{
          "keys" => %{
            ("ed25519:" <> alice_msk_b64) => alice_msk_b64
          },
          "usage" => ["master"],
          "user_id" => alice
        }

        {:ok, alice_usk} =
          %{
            "keys" => %{
              ("ed25519:" <> alice_usk_b64) => alice_usk_b64
            },
            "usage" => ["user_signing"],
            "user_id" => alice
          }
          |> Polyjuice.Util.JSON.sign(alice, alice_msk_signing_key)

        {:ok, alice_ssk} =
          %{
            "keys" => %{
              ("ed25519:" <> alice_ssk_b64) => alice_ssk_b64
            },
            "usage" => ["self_signing"],
            "user_id" => alice
          }
          |> Polyjuice.Util.JSON.sign(alice, alice_msk_signing_key)

        :ok =
          Polyjuice.Server.Protocols.DeviceKey.set_cross_signing_keys(test_server, alice, %{
            "master_key" => alice_msk,
            "user_signing_key" => alice_usk,
            "self_signing_key" => alice_ssk
          })

        keys =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(test_server, alice, %{alice => []}, nil)

        assert keys == %{
                 "device_keys" => %{alice => %{alice_device_id => alice_self_signed_device_keys}},
                 "master_keys" => %{alice => alice_msk},
                 "self_signing_keys" => %{alice => alice_ssk},
                 "user_signing_keys" => %{alice => alice_usk}
               }

        {:ok, alice_device_signed_msk} =
          Polyjuice.Util.JSON.sign(alice_msk, alice, alice_device_signing_key)

        {:ok, alice_master_signed_device_keys} =
          Polyjuice.Util.JSON.sign(alice_unsigned_device_keys, alice, alice_ssk_signing_key)

        Polyjuice.Server.Protocols.DeviceKey.add_signatures(test_server, alice, %{
          alice => %{
            alice_device_id => alice_master_signed_device_keys,
            alice_msk_b64 => alice_device_signed_msk
          }
        })

        keys =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(test_server, alice, %{alice => []}, nil)

        alice_device_verify_key = %Polyjuice.Util.Ed25519.VerifyKey{
          key: alice_device_pubkey,
          id: alice_device_id
        }

        alice_ssk_verify_key = %Polyjuice.Util.Ed25519.VerifyKey{
          key: alice_ssk_pubkey,
          id: alice_ssk_b64
        }

        # make sure the signatures were added, and none were dropped
        assert Polyjuice.Util.JSON.signed?(
                 keys["device_keys"][alice][alice_device_id],
                 alice,
                 alice_device_verify_key
               )

        assert Polyjuice.Util.JSON.signed?(
                 keys["device_keys"][alice][alice_device_id],
                 alice,
                 alice_ssk_verify_key
               )

        assert Polyjuice.Util.JSON.signed?(
                 keys["master_keys"][alice],
                 alice,
                 alice_device_verify_key
               )

        {bob_msk_pubkey, bob_msk_privkey} =
          {<<30, 40, 71, 233, 213, 21, 249, 179, 77, 14, 52, 233, 24, 77, 30, 158, 29, 92, 197,
             208, 45, 142, 58, 25, 124, 204, 198, 41, 61, 42, 116, 220>>,
           <<226, 57, 88, 151, 30, 177, 176, 5, 171, 75, 2, 45, 220, 93, 0, 142, 170, 164, 73,
             139, 222, 200, 72, 53, 133, 182, 251, 71, 147, 121, 164, 19>>}

        bob_msk_b64 = Base.encode64(bob_msk_pubkey, padding: false)

        bob_msk_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
          key: bob_msk_privkey,
          id: bob_msk_b64
        }

        {bob_usk_pubkey, _bob_usk_privkey} =
          {<<31, 243, 157, 75, 41, 243, 85, 237, 86, 114, 28, 9, 147, 107, 236, 104, 92, 73, 142,
             83, 124, 214, 104, 128, 187, 2, 178, 209, 101, 144, 88, 160>>,
           <<232, 41, 36, 96, 120, 229, 29, 52, 2, 5, 246, 75, 93, 236, 118, 201, 205, 19, 204,
             164, 87, 118, 24, 7, 12, 134, 107, 223, 12, 85, 235, 87>>}

        bob_usk_b64 = Base.encode64(bob_usk_pubkey, padding: false)

        {bob_ssk_pubkey, _bob_ssk_privkey} =
          {<<203, 213, 255, 6, 202, 183, 143, 58, 41, 166, 9, 159, 45, 65, 226, 100, 79, 141, 148,
             15, 96, 87, 204, 8, 176, 227, 48, 201, 24, 123, 137, 251>>,
           <<146, 96, 15, 245, 221, 222, 95, 209, 107, 214, 51, 114, 244, 66, 129, 33, 129, 24,
             34, 47, 108, 50, 145, 23, 181, 10, 236, 45, 109, 4, 20, 217>>}

        bob_ssk_b64 = Base.encode64(bob_ssk_pubkey, padding: false)

        bob_msk = %{
          "keys" => %{
            ("ed25519:" <> bob_msk_b64) => bob_msk_b64
          },
          "usage" => ["master"],
          "user_id" => bob
        }

        {:ok, bob_usk} =
          %{
            "keys" => %{
              ("ed25519:" <> bob_usk_b64) => bob_usk_b64
            },
            "usage" => ["user_signing"],
            "user_id" => bob
          }
          |> Polyjuice.Util.JSON.sign(bob, bob_msk_signing_key)

        {:ok, bob_ssk} =
          %{
            "keys" => %{
              ("ed25519:" <> bob_ssk_b64) => bob_ssk_b64
            },
            "usage" => ["self_signing"],
            "user_id" => bob
          }
          |> Polyjuice.Util.JSON.sign(bob, bob_msk_signing_key)

        :ok =
          Polyjuice.Server.Protocols.DeviceKey.set_cross_signing_keys(test_server, bob, %{
            "master_key" => bob_msk,
            "user_signing_key" => bob_usk,
            "self_signing_key" => bob_ssk
          })

        {:ok, alice_signed_bob_msk} =
          Polyjuice.Util.JSON.sign(bob_msk, alice, alice_usk_signing_key)

        Polyjuice.Server.Protocols.DeviceKey.add_signatures(test_server, alice, %{
          bob => %{
            bob_msk_b64 => alice_signed_bob_msk
          }
        })

        # alice should be able to see her signature on Bob's master key, and
        # see bob's self-signing key, but not his user-signing key
        keys =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(test_server, alice, %{bob => []}, nil)

        alice_usk_verify_key = %Polyjuice.Util.Ed25519.VerifyKey{
          key: alice_usk_pubkey,
          id: alice_usk_b64
        }

        assert Polyjuice.Util.JSON.signed?(keys["master_keys"][bob], alice, alice_usk_verify_key)
        assert keys["user_signing_keys"] == nil or keys["user_signing_keys"][bob] == nil
        assert keys["self_signing_keys"][bob] == bob_ssk

        # bob should be able to see his own keys (but not Alice's signature on
        # his master key), Alice's master key (with signature from her device),
        # Alice's device key (with signature from her ssk), and Alice's SSK
        # (with signature from her msk)
        keys =
          Polyjuice.Server.Protocols.DeviceKey.query_keys(
            test_server,
            bob,
            %{alice => [], bob => []},
            nil
          )

        assert keys["master_keys"][bob] == bob_msk
        assert keys["user_signing_keys"][bob] == bob_usk
        assert keys["self_signing_keys"][bob] == bob_ssk

        assert Polyjuice.Util.JSON.signed?(
                 keys["device_keys"][alice][alice_device_id],
                 alice,
                 alice_device_verify_key
               )

        assert Polyjuice.Util.JSON.signed?(
                 keys["device_keys"][alice][alice_device_id],
                 alice,
                 alice_ssk_verify_key
               )

        assert Polyjuice.Util.JSON.signed?(
                 keys["master_keys"][alice],
                 alice,
                 alice_device_verify_key
               )

        assert keys["self_signing_keys"][alice] == alice_ssk
        assert keys["user_signing_keys"][alice] == nil

        events = BaseTestServer.get_test_state(test_server, &Map.get(&1, :events, []))

        assert events == [
                 {:add_signatures, alice,
                  %{
                    bob => %{
                      bob_msk_b64 => alice_signed_bob_msk
                    }
                  }},
                 {:set_cross_signing_keys, bob,
                  %{
                    "master_key" => bob_msk,
                    "user_signing_key" => bob_usk,
                    "self_signing_key" => bob_ssk
                  }},
                 {:add_signatures, alice,
                  %{
                    alice => %{
                      alice_device_id => alice_master_signed_device_keys,
                      alice_msk_b64 => alice_device_signed_msk
                    }
                  }},
                 {:set_cross_signing_keys, alice,
                  %{
                    "master_key" => alice_msk,
                    "user_signing_key" => alice_usk,
                    "self_signing_key" => alice_ssk
                  }},
                 {:set_device_keys, alice, alice_self_signed_device_keys},
                 {:login, alice, alice_device_id, alice_access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end
end
