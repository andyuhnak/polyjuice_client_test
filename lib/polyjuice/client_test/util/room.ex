# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.Room do
  @moduledoc """
  Utility functions for rooms
  """

  @doc """
  Create a room on the server.

  Supported options are:

  - `name:` the room name
  - `topic:` the room topic
  - `invite:` a list of user IDs to invite to the room
  - `join_rules:` the join rule for the room (defaults to "invite")
  - `history_visibility:` the history visibility for the room (defaults to
    "shared")
  - `power_levels:` the contents of the power levels event (defaults to the
    room creator having PL 100)
  - `encryption:` enable encryption with the given algorithm and (optional)
    parameters.  The value can either be just the algorithm name (as a string),
    or a tuple of the form `{algorithm, params}`, where `params` is an
    enumerable that yields `{key, value}` pairs.  For example,
    `{"m.megolm.v1.aes-sha2", rotation_period_msgs: 1}`
  """
  @spec create(
          Polyjuice.Server.Protocols.Room.t(),
          String.t(),
          Keyword.t()
        ) :: {:ok, String.t()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def create(server, user_id, opts \\ []) do
    initial_state =
      [
        [
          {
            user_id,
            "m.room.create",
            "",
            %{
              "creator" => user_id,
              "room_version" => "5"
            }
          },
          {
            user_id,
            "m.room.member",
            user_id,
            %{
              "membership" => "join"
            }
          },
          {
            user_id,
            "m.room.power_levels",
            "",
            Keyword.get(opts, :power_levels, %{
              "users" => %{
                user_id => 100
              }
            })
          },
          {
            user_id,
            "m.room.join_rules",
            "",
            %{
              "join_rule" => Keyword.get(opts, :join_rule, "invite")
            }
          },
          {
            user_id,
            "m.room.history_visibility",
            "",
            %{
              "history_visibility" => Keyword.get(opts, :history_visibilty, "shared")
            }
          }
        ],
        case Keyword.get(opts, :encryption) do
          nil ->
            []

          alg when is_binary(alg) ->
            [
              {
                user_id,
                "m.room.encryption",
                "",
                %{
                  "algorithm" => alg
                }
              }
            ]

          {alg, opts} when is_binary(alg) and is_list(opts) ->
            [
              {
                user_id,
                "m.room.encryption",
                "",
                Enum.map(opts, fn {name, val} -> {to_string(name), val} end)
                |> Map.new()
                |> Map.put("algorithm", alg)
              }
            ]
        end,
        case Keyword.fetch(opts, :name) do
          {:ok, name} ->
            [
              {
                user_id,
                "m.room.name",
                "",
                %{
                  "name" => name
                }
              }
            ]

          :error ->
            []
        end,
        case Keyword.fetch(opts, :topic) do
          {:ok, topic} ->
            [
              {
                user_id,
                "m.room.topic",
                "",
                %{
                  "topic" => topic
                }
              }
            ]

          :error ->
            []
        end,
        Keyword.get(opts, :invite, [])
        |> Enum.map(fn user ->
          {
            user_id,
            "m.room.member",
            user,
            %{
              "membership" => "invite"
            }
          }
        end)
      ]
      |> Enum.concat()

    Polyjuice.Server.Protocols.Room.create(server, initial_state)
  end

  @doc """
  Get the textual portion of the event.
  """
  @spec get_event_text(Polyjuice.Util.event()) :: String.t() | nil
  def get_event_text(event) do
    # FIXME: eventually, we want to handle extensible events
    event["content"]["body"]
  end
end
