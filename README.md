# Polyjuice Client Test

Polyjuice Client Test is a tool for testing Matrix clients.  Each test is run
on its own homeserver, and the tests can customize the homeserver behaviour as
needed.  This allows the tests to test things that may be harder to test with a
normal homeserver, for example, sending malformed data that a normal homeserver
would not send, or simulating network or server issues.

## Building

To build, in addition to Elixir, you will need Node.js, Yarn, and a recent
version of Rust.  You can use one of the images from
[registry.gitlab.com/polyjuice/container_images/elixir-rust-node](https://gitlab.com/polyjuice/container_images/container_registry/2760507),
which has those available.

Then run `mix deps.get` to install the dependencies, and `mix compile` to
build.

Note: on M1 Macs, you may need to edit (or create) `~/.cargo/config` to
include (ref: [rustler#403](https://github.com/rusterlium/rustler/issues/403)):

```
[target.'cfg(target_os = "macos")']
rustflags = [
    "-C", "link-arg=-undefined",
    "-C", "link-arg=dynamic_lookup",
]
```

## Configuration and running

Create a `config/config.exs` file; you can use `config/config.sample.exs` as a
starting point.

The server must use HTTPS (either natively, or via a reverse proxy).  The
server must use a certificate that is valid for the dashboard host name and all
test domains (e.g. `dashboard.example.org` and `*.tests.example.org`).  If
using a reverse proxy, it will need to be configured to pass requests for the
dashboard host name and all test domains to the server.

Finally, start the server by running `mix run --no-halt`.

### Running a development environment

When running a development environment, you may not have a domain name or an
SSL certificate.  You may be able to use the `*.localhost` namespace for the
test domains, if your operating system supports it (e.g. use `test.localhost`
as the test domain) -- `*.localhost` should point to 127.0.0.1.  (To see if your
operating system supports the `*.localhost` namespace, try running `ping
foo.localhost`.  If it succeeds, then it is supported.)  You may also
need to add the port number to the domains, and you will need to add an
`address:` option to `server_options` so that it will not try to redirect to
HTTPS.  For example, the following `config/config.exs` can be used.

```elixir
use Mix.Config

config :polyjuice_client_test,
  server_options: [
    "test.localhost:4000",
    ["Polyjuice.ClientTest.Test.*"],
    address: "http://test.localhost:4000"
  ]
```

If your operating system does not support the `*.localhost` namespace, then you
can use `*.local.uhoreg.ca` instead, which will point to 127.0.0.1.  For
example, the following `config/config.exs` can be used.

```elixir
use Mix.Config

config :polyjuice_client_test,
  server_options: [
    "localhost:4000",
    ["Polyjuice.ClientTest.Test.*"],
    address: "http://localhost:4000",
    test_domain: "local.uhoreg.ca:4000"
  ]
```

Without an SSL certificate and HTTPS, clients will be unable to perform
discovery, which means you will need to manually specify the homeserver
location when running tests, rather than simply entering the user ID.  The
homeserver location is simply the servername portion of the user ID, as an
`http://` URL.  For example, if a test asks you to log in as
`@test:abcdef.local.uhoreg.ca:4000`, then you would use `test` as the username,
and `http://abcdef.local.uhoreg.ca:4000` as the homeserver URL.
