# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.PushRule do
  @behaviour Polyjuice.Server.Protocols.Helper.PushRule

  def get_push_rules({_user_id, _scope}, _from, state) do
    # dummy implementation for now
    {:reply,
     %{
       "global" => %{
         "content" => [],
         "override" => [],
         "room" => [],
         "sender" => [],
         "underride" => []
       }
     }, state}
  end

  def get_push_rule({_user_id, _scope, _kind, _rule_id}, _from, state) do
    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Not found",
        plug_status: 404,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
      }}, state}
  end
end
