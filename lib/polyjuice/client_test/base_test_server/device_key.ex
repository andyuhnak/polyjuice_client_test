# Copyright 2021-2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.DeviceKey do
  @behaviour Polyjuice.Server.Protocols.Helper.DeviceKey

  @type device_keys_map() :: %{optional(String.t()) => %{String.t() => map()}}

  # map from {user_id, device_id} to algorithm to queue of {key ID, one-time key}
  @type one_time_keys_map() :: %{
          optional({String.t(), String.t()}) => %{String.t() => :queue.t({String.t(), map()})}
        }

  @type cross_signing_keys_map() :: %{optional(String.t()) => %{String.t() => map()}}

  @type changed_keys_stream :: list({pos_integer, String.t()})

  @type required_state() :: %{
          device_keys: device_keys_map(),
          one_time_keys: one_time_keys_map(),
          # FIXME: add fallback keys
          cross_signing_keys: cross_signing_keys_map(),
          changed_keys_stream: changed_keys_stream()
        }

  @type optional_state_rooms() :: %{
          room_event_stream: Polyjuice.ClientTest.BaseTestServer.Room.room_event_stream(),
          room_state: Polyjuice.ClientTest.BaseTestServer.Room.room_state_map(),
          rooms_by_user: Polyjuice.ClientTest.BaseTestServer.Room.rooms_by_user_map()
        }

  @type optional_state_pub_sub() :: %{
          subscribers: %{optional(any) => Polyjuice.Server.Protocols.Subscriber.t()}
        }

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def set_device_keys({user_id, device_id, device_keys}, from, state) do
    stream_pos =
      case state.changed_keys_stream do
        [{pos, _} | _] -> pos + 1
        [] -> 1
      end

    new_device_keys =
      Map.get(state.device_keys, user_id, %{})
      |> Map.put(device_id, device_keys)
      |> (&Map.put(state.device_keys, user_id, &1)).()

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:set_device_keys, user_id, device_keys},
        state
      )

    state = %{
      state
      | device_keys: new_device_keys,
        changed_keys_stream: [{stream_pos, user_id} | state.changed_keys_stream]
    }

    {:reply, :ok, _state} =
      if Map.has_key?(state, :subscribers) do
        # FIXME: also publish to users we share an encrypted room with
        Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
          {{:device_keys_changed, stream_pos, user_id}, [user_id]},
          from,
          state
        )
      else
        {:reply, :ok, state}
      end
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def add_one_time_keys({user_id, device_id, keys}, _from, state) do
    # FIXME: check if the keys have already been uploaded, and are different
    otk_for_device = Map.get(state.one_time_keys, {user_id, device_id}, %{})

    new_otk_for_device =
      Enum.reduce(keys, otk_for_device, fn {prefixed_key_id, key}, acc ->
        [algorithm, key_id] = String.split(prefixed_key_id, ":", parts: 2)

        Map.get(acc, algorithm, :queue.new())
        |> (&:queue.in({key_id, key}, &1)).()
        |> (&Map.put(acc, algorithm, &1)).()
      end)

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:add_one_time_keys, user_id, device_id, keys},
        state
      )

    {:reply, :ok,
     %{
       state
       | one_time_keys: Map.put(state.one_time_keys, {user_id, device_id}, new_otk_for_device)
     }}
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def set_cross_signing_keys({user_id, keys}, from, state) do
    new_cross_signing_keys =
      Map.get(state.cross_signing_keys, user_id, %{})
      |> Map.merge(keys)
      |> (&Map.put(state.cross_signing_keys, user_id, &1)).()

    stream_pos =
      case state.changed_keys_stream do
        [{pos, _} | _] -> pos + 1
        [] -> 1
      end

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:set_cross_signing_keys, user_id, keys},
        state
      )

    state = %{
      state
      | cross_signing_keys: new_cross_signing_keys,
        changed_keys_stream: [{stream_pos, user_id} | state.changed_keys_stream]
    }

    {:reply, :ok, _state} =
      if Map.has_key?(state, :subscribers) do
        # FIXME: also publish to users we share an encrypted room with
        Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
          {{:device_keys_changed, stream_pos, user_id}, [user_id]},
          from,
          state
        )
      else
        {:reply, :ok, state}
      end
  end

  defp merge_signatures(orig_sigs, new_sigs) do
    Enum.reduce(new_sigs, orig_sigs, fn {user_id, new_user_sigs}, acc ->
      case Map.fetch(acc, user_id) do
        {:ok, orig_user_sigs} ->
          Map.put(acc, user_id, Map.merge(orig_user_sigs, new_user_sigs))

        :error ->
          Map.put(acc, user_id, new_user_sigs)
      end
    end)
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def add_signatures({user_id, signatures}, from, state) do
    {new_device_keys, new_cross_signing_keys, failures} =
      Enum.reduce(
        signatures,
        {state.device_keys, state.cross_signing_keys, %{}},
        fn {user_id, user_sigs}, {device_keys, cross_signing_keys, failures} ->
          {user_device_keys, user_cross_signing_keys, failures} =
            Enum.reduce(
              user_sigs,
              {Map.get(device_keys, user_id, %{}), Map.get(cross_signing_keys, user_id, %{}),
               failures},
              fn {device_id, sig}, {user_device_keys, user_cross_signing_keys, failures} ->
                case Map.fetch(user_device_keys, device_id) do
                  {:ok, device_key} ->
                    # check that the key data is the same as what we have stored
                    if Enum.all?(sig, fn {key, value} ->
                         key == "signatures" or {:ok, value} == Map.fetch(device_key, key)
                       end) do
                      {
                        Map.get(device_key, "signatures", %{})
                        |> merge_signatures(Map.get(sig, "signatures", %{}))
                        |> (&Map.put(device_key, "signatures", &1)).()
                        |> (&Map.put(user_device_keys, device_id, &1)).(),
                        user_cross_signing_keys,
                        failures
                      }
                    else
                      # FIXME: should emit an error
                      {user_device_keys, user_cross_signing_keys, failures}
                    end

                  :error ->
                    # it's not a normal device, so check the cross-signing keys
                    # FIXME: support signatures of other user's keys
                    new_cross_signing_keys =
                      Enum.reduce(
                        Map.keys(user_cross_signing_keys),
                        user_cross_signing_keys,
                        fn key_type, acc ->
                          xsigning_key = Map.get(user_cross_signing_keys, key_type)

                          if Enum.all?(sig, fn {key, value} ->
                               key == "signatures" or {:ok, value} == Map.fetch(xsigning_key, key)
                             end) do
                            Map.get(xsigning_key, "signatures", %{})
                            |> merge_signatures(Map.get(sig, "signatures", %{}))
                            |> (&Map.put(xsigning_key, "signatures", &1)).()
                            |> (&Map.put(acc, key_type, &1)).()
                          else
                            # FIXME: we should check if the "key" property is the
                            # same, and emit an error if so, because that means
                            # that we're trying to add a signature for a key, but
                            # we don't have the same contents
                            acc
                          end
                        end
                      )

                    {user_device_keys, new_cross_signing_keys, failures}
                end
              end
            )

          {
            if user_device_keys == %{} do
              device_keys
            else
              Map.put(device_keys, user_id, user_device_keys)
            end,
            if user_cross_signing_keys == %{} do
              cross_signing_keys
            else
              Map.put(cross_signing_keys, user_id, user_cross_signing_keys)
            end,
            failures
          }
        end
      )

    stream_pos =
      case state.changed_keys_stream do
        [{pos, _} | _] -> pos + 1
        [] -> 1
      end

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:add_signatures, user_id, signatures},
        state
      )

    state = %{
      state
      | device_keys: new_device_keys,
        cross_signing_keys: new_cross_signing_keys,
        changed_keys_stream: [{stream_pos, user_id} | state.changed_keys_stream]
    }

    {:reply, :ok, state} =
      if Map.has_key?(state, :subscribers) do
        # FIXME: also publish to users we share an encrypted room with
        Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
          {{:device_keys_changed, stream_pos, user_id}, [user_id]},
          from,
          state
        )
      else
        {:reply, :ok, state}
      end

    {:reply, failures, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def claim_key({one_time_keys}, _from, state) do
    {res, state} =
      Enum.reduce(one_time_keys, {%{}, state}, fn {user_id, device_map}, acc ->
        Enum.reduce(device_map, acc, fn {device_id, algorithm}, {res, state} ->
          with {:ok, device_otks} <- Map.fetch(state.one_time_keys, {user_id, device_id}),
               {:ok, queue} <- Map.fetch(device_otks, algorithm),
               {{:value, {key_id, key}}, queue} <- :queue.out(queue) do
            res =
              Map.get(res, user_id, %{})
              |> Map.put(device_id, %{"#{algorithm}:#{key_id}" => key})
              |> (&Map.put(res, user_id, &1)).()

            new_device_one_time_keys = Map.put(device_otks, algorithm, queue)

            new_one_time_keys =
              Map.put(state.one_time_keys, {user_id, device_id}, new_device_one_time_keys)

            state = %{state | one_time_keys: new_one_time_keys}
            {res, state}
          else
            # FIXME: fallback key
            _ -> {res, state}
          end
        end)
      end)

    # FIXME: notify devices that their keys have been claimed

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:claim_one_time_keys, one_time_keys, res},
        state
      )

    {:reply, res, state}
  end

  defp strip_signatures(key, user_ids) do
    # only send signatures made by certain users
    # this doesn't quite match what the spec says, but is close enough for now,
    # and should give the same results if clients upload sane things
    # FIXME: make /keys/query and /signatures/upload actually match the spec
    case Map.fetch(key, "signatures") do
      {:ok, signatures} ->
        signatures = Map.take(signatures, user_ids)

        if signatures == %{} do
          Map.delete(key, "signatures")
        else
          Map.put(key, "signatures", signatures)
        end

      :error ->
        key
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def query_keys({user_id, device_keys_query, _token}, _from, state) do
    # FIXME: what do we do with token?

    device_keys =
      Enum.reduce(device_keys_query, %{}, fn {key_user_id, devices}, acc ->
        case Map.fetch(state.device_keys, key_user_id) do
          {:ok, keys_for_user} ->
            if devices == [] do
              Enum.reduce(keys_for_user, acc, fn {device_id, keys}, acc ->
                keys = strip_signatures(keys, [user_id, key_user_id])

                Map.get(acc, key_user_id, %{})
                |> Map.put(device_id, keys)
                |> (&Map.put(acc, key_user_id, &1)).()
              end)
            else
              Enum.reduce(devices, acc, fn device_id, acc ->
                case Map.fetch(keys_for_user, device_id) do
                  {:ok, keys} ->
                    keys = strip_signatures(keys, [user_id, key_user_id])

                    Map.get(acc, key_user_id, %{})
                    |> Map.put(device_id, keys)
                    |> (&Map.put(acc, key_user_id, &1)).()

                  _ ->
                    acc
                end
              end)
            end

          :error ->
            acc
        end
      end)

    cross_signing_keys =
      Enum.reduce(device_keys_query, %{}, fn {key_user_id, _}, acc ->
        case Map.fetch(state.cross_signing_keys, key_user_id) do
          {:ok, keys_for_user} ->
            Enum.reduce(keys_for_user, acc, fn {key_type, key}, acc ->
              if user_id == key_user_id or key_type == "master_key" or
                   key_type == "self_signing_key" do
                plural_key_type = key_type <> "s"
                key = strip_signatures(key, [user_id, key_user_id])

                Map.get(acc, plural_key_type, %{})
                |> Map.put(key_user_id, key)
                |> (&Map.put(acc, plural_key_type, &1)).()
              else
                acc
              end
            end)

          :error ->
            acc
        end
      end)

    {:reply, Map.merge(cross_signing_keys, %{"device_keys" => device_keys}), state}
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def get_changes({user_id, from_pos, to_pos}, _from, state) do
    device_stream_from = Polyjuice.Server.SyncToken.get_component(from_pos, :k)

    if match?(%{room_event_stream: _, room_state: _, rooms_by_user: _}, state) do
      room_stream_from = Polyjuice.Server.SyncToken.get_component(from_pos, :r)

      {room_stream_to, device_stream_to} =
        if to_pos == nil do
          {nil, nil}
        else
          {Polyjuice.Server.SyncToken.get_component(to_pos, :r),
           Polyjuice.Server.SyncToken.get_component(to_pos, :k)}
        end

      changed =
        Enum.reduce_while(
          state.changed_keys_stream,
          MapSet.new(),
          fn {pos, update_user}, acc ->
            cond do
              pos <= device_stream_from ->
                {:halt, acc}

              device_stream_to != nil and pos > device_stream_to ->
                {:cont, acc}

              true ->
                if Polyjuice.ClientTest.BaseTestServer.Room.shared_room?(
                     user_id,
                     update_user,
                     state
                   ) do
                  {:cont, MapSet.put(acc, update_user)}
                else
                  {:cont, acc}
                end
            end
          end
        )

      {changed, left} =
        Enum.reduce_while(
          state.room_event_stream,
          {changed, MapSet.new()},
          fn {pos, events}, acc ->
            cond do
              pos <= room_stream_from ->
                {:halt, acc}

              room_stream_to != nil and pos > room_stream_to ->
                {:cont, acc}

              true ->
                Enum.reduce(
                  events,
                  acc,
                  fn event, {changed, left} = acc ->
                    if Map.get(event, "type") == "m.room.member" and
                         Map.has_key?(event, "state_key") do
                      # FIXME: if the room is not encrypted, we can skip these
                      state_key = Map.get(event, "state_key")

                      prev_membership =
                        Map.get(event, "unsigned", %{})
                        |> Map.get("prev_content", %{})
                        |> Map.get("membership")

                      membership =
                        Map.get(event, "content", %{})
                        |> Map.get("membership")

                      case membership do
                        "leave" ->
                          if prev_membership == "join" do
                            if Polyjuice.ClientTest.BaseTestServer.Room.shared_room?(
                                 user_id,
                                 state_key,
                                 state
                               ) do
                              # the user left the room, but we still share a room
                              # with them, so no changes
                              acc
                            else
                              {MapSet.delete(changed, state_key), MapSet.put(left, state_key)}
                            end
                          else
                          end

                        "join" ->
                          if prev_membership != "join" and prev_membership != "invite" and
                               not MapSet.member?(left, state_key) do
                            # the user joined a room with us, and we didn't
                            # previously determine that we no longer share a room
                            # with them (i.e. they haven't subsequently left)
                            # FIXME: we don't need to add them to `changed` if they
                            # previously shared an encrypted room with us
                            {MapSet.put(changed, state_key), left}
                          else
                            acc
                          end

                        "invite" ->
                          if prev_membership != "join" and prev_membership != "invite" and
                               not MapSet.member?(left, state_key) do
                            {MapSet.put(changed, state_key), left}
                          else
                            acc
                          end

                        _ ->
                          acc
                      end
                    else
                      acc
                    end
                  end
                )
            end
          end
        )

      {:reply, {MapSet.to_list(changed), MapSet.to_list(left)}, state}
    else
      # we don't support rooms, so all just check if our own devices changed
      device_stream_to =
        if to_pos == nil do
          nil
        else
          Polyjuice.Server.SyncToken.get_component(to_pos, :k)
        end

      changed =
        Enum.reduce_while(
          state.changed_keys_stream,
          false,
          fn {pos, update_user}, _ ->
            cond do
              pos <= device_stream_from ->
                {:halt, false}

              device_stream_to != nil and pos > device_stream_to ->
                {:cont, false}

              update_user == user_id ->
                {:halt, true}

              true ->
                {:cont, false}
            end
          end
        )

      {:reply, {if(changed, do: [user_id], else: []), []}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.DeviceKey
  def key_counts({user_id, device_id}, _from, state) do
    otk_counts =
      Map.get(state.one_time_keys, {user_id, device_id}, %{})
      |> Enum.map(fn {algorithm, keys} -> {algorithm, :queue.len(keys)} end)
      |> Map.new()

    {:reply, %{one_time_keys: otk_counts, unused_fallback_keys: []}, state}
  end
end
