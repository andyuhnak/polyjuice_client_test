# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.Routes do
  @moduledoc false
  use Plug.Router

  plug(:check_override)
  plug(:match)
  plug(:dispatch, builder_opts())

  def check_override(conn, opts) do
    server =
      Polyjuice.ClientTest.DashboardServer.get_server_for_host(
        Polyjuice.ClientTest.DashboardServer,
        conn.host
      )

    if !match?(%Polyjuice.ClientTest.DashboardServer{}, server) and
         Polyjuice.ClientTest.OverrideEndPoints.impl_for(server) != nil do
      Polyjuice.ClientTest.OverrideEndPoints.call(server, conn, opts)
    else
      conn
    end
  end

  match "/" do
    dashboard_server =
      Polyjuice.ClientTest.DashboardServer.get_server_for_host(
        Polyjuice.ClientTest.DashboardServer,
        ""
      )

    servername = dashboard_server.servername

    if conn.host == servername or "#{conn.host}:#{conn.port}" == servername do
      conn
      |> put_resp_content_type("text/html; charset=utf-8")
      |> send_file(200, Application.app_dir(:polyjuice_client_test, "priv/static/index.html"))
    else
      conn
      |> put_resp_header("Location", dashboard_server.address)
      |> send_resp(301, dashboard_server.address)
    end
  end

  post "/_matrix/client/:ver/rooms/:room_id/read_markers" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, "{}")
  end

  post "/_matrix/client/:ver/rooms/:room_id/receipt/:type/:event_id" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, "{}")
  end

  put "/_matrix/client/:ver/presence/:user_id/status" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, "{}")
  end

  put "/_matrix/client/:ver/rooms/:room_id/typing/:user_id" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, "{}")
  end

  get "/_matrix/client/:ver/profile/:user_id/displayname" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, ~s({"displayname":"Bob"}))
  end

  get "/_matrix/client/:ver/profile/:user_id/avatar_url" do
    # dummy implementation for now, to avoid getting flooded with errors
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> put_resp_content_type("application/json")
    |> send_resp(200, ~s({"avatar_url":"image_url"}))
  end

  options "/_matrix/client/*path" do
    conn
    |> Polyjuice.Server.Plug.Client.cors([])
    |> send_resp(204, "")
  end

  match "/_matrix/client/*path" do
    server =
      Polyjuice.ClientTest.DashboardServer.get_server_for_host(
        Polyjuice.ClientTest.DashboardServer,
        conn.host
      )

    cond do
      server ->
        conn =
          if !match?(%Polyjuice.ClientTest.DashboardServer{}, server) do
            dashboard_server =
              Polyjuice.ClientTest.DashboardServer.get_server_for_host(
                Polyjuice.ClientTest.DashboardServer,
                ""
              )

            [test_id | _] = String.split(conn.host, ".", parts: 2)

            start_time = :erlang.system_time(:millisecond)

            request = %{
              "method" => conn.method,
              "path" => conn.request_path,
              "query_string" => conn.query_string,
              "headers" => Enum.map(conn.req_headers, fn {name, value} -> [name, value] end),
              "timestamp" => start_time
            }

            # FIXME: add request body as body if method is POST or PUT
            {:ok, event_id} =
              Polyjuice.ClientTest.DashboardServer.send_dashboard_event(
                dashboard_server,
                test_id,
                %{
                  "type" => "ca.uhoreg.polyjuice.client_test.request_log",
                  "content" => %{
                    "request" => request
                  }
                }
              )

            Plug.Conn.register_before_send(conn, fn conn ->
              end_time = :erlang.system_time(:millisecond)

              response = %{
                "status" => conn.status,
                "headers" => Enum.map(conn.resp_headers, fn {name, value} -> [name, value] end),
                "timestamp" => end_time,
                "duration" => end_time - start_time
              }

              response =
                if conn.state == :set or conn.state == :sent do
                  Map.put(response, "content", conn.resp_body)
                else
                  response
                end

              Polyjuice.ClientTest.DashboardServer.send_dashboard_event(
                dashboard_server,
                test_id,
                %{
                  "type" => "ca.uhoreg.polyjuice.client_test.request_log",
                  "content" => %{
                    "m.relates_to" => %{
                      "event_id" => event_id,
                      "rel_type" => "m.replace"
                    },
                    "m.new_content" => %{
                      "request" => request,
                      "response" => response
                    }
                  }
                }
              )

              conn
            end)
          else
            conn
          end

        Plug.forward(conn, path, Polyjuice.Server.Plug.Client, [{:server, server} | opts])

      match?(["_matrix", "client", _, "logout"], conn.path_info) and conn.method == "POST" ->
        conn
        |> Polyjuice.Server.Plug.Client.cors([])
        |> put_resp_content_type("application/json")
        |> send_resp(200, "{}")

      true ->
        conn
        |> Polyjuice.Server.Plug.Client.cors([])
        |> put_resp_content_type("application/json")
        |> send_resp(403, ~s({"errcode":"M_UNKNOWN_TOKEN","error":"Unknown token"}))
    end
  end

  match ".well-known/matrix/*path" do
    server =
      Polyjuice.ClientTest.DashboardServer.get_server_for_host(
        Polyjuice.ClientTest.DashboardServer,
        conn.host
      )

    if server do
      Plug.forward(conn, path, Polyjuice.Server.Plug.WellKnown, [{:server, server} | opts])
    else
      conn
      |> put_resp_content_type("text/plain")
      |> send_resp(404, "Not found")
    end
  end

  match _ do
    Plug.run(conn, [
      {Plug.Static, at: "/", from: {:polyjuice_client_test, "priv/static"}},
      &(&1
        |> put_resp_content_type("text/plain")
        |> send_resp(404, "Not found"))
    ])
  end
end
