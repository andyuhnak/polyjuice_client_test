# Writing tests

## Getting started

Read the [README](readme.html) for information on how to configure, build, and
run.

## Creating a test module

- create a file in `lib/polyjuice/client_test/test` -- you can copy one of the
  existing files as a sample.  The file should be named using snake_case, and
  end with an extension of `.ex`.
- the file should have the following form:
  ```elixir
  # copyright header

  defmodule Polyjuice.ClientTest.Test.[ModuleName] do
    @moduledoc """
    Short test description (usually just one line)

    Longer test description (optional, can be multiple lines)
    """

    use Polyjuice.ClientTest.TestHelper

    def test_script(server) do
      ...
    end
  end
  ```
  where `[ModuleName]` is the test name in CamelCase (the same name as the file
  name, but with a different case).  The `use Polyjuice.ClientTest.TestHelper`
  line can take arguments.  See the `Polyjuice.ClientTest.TestHelper`
  documentation for more information.
- fill in the `test_script` function
  - see the [Test script utility structs](#test-script-utility-structs)
    section below
  - look at the ["Writing
    tests"](Polyjuice.ClientTest.BaseTestServer.html#module-writing-tests)
    section of the `Polyjuice.ClientTest.BaseTestServer` module documentation.
  - explore the documentation for
    `Polyjuice.ClientTest.BaseTestServer`,
    the
    `Polyjuice.ClientTest.Util.*`
    modules, and the
    [`Polyjuice.Server.Protocols.*`](https://polyjuice.gitlab.io/polyjuice_server/)
    modules.
- you may need to use event handlers to handle things that do not fit neatly
  into the test script, such as things that may happen at unpredictable times.
  To use event handlers, add an `event_handlers:` option to the `use
  Polyjuice.ClientTest.TestHelper` macro call.

## Test script utility structs

The `Polyjuice.ClientTest.Util.*` modules implement several structs
that are helpful for writing tests.  The structs represent different
objects that can be modified by calling functions on them.  Since
these functions generally also return a struct with the appropriate
modifications made, they can be chained together using pipes.  For
example, one can write:

```
alias Polyjuice.ClientTest.Util

user =
  Util.User.create(server, "username", "password")
  |> Util.SSSS.create_default()
  |> Util.CrossSigning.generate_keys()
  |> Util.CrossSigning.store_in_ssss()
```

to:

- create a new user,
- create a default SSSS storage for the user,
- generate cross-signing keys for the user, and
- store the cross-signing private keys in SSSS.

## Limitations

Not all homeserver functionality is implemented yet.  Some notable limitations:

- backfilling in rooms (`/messages` endpoint) is not yet supported
- filters don't work (e.g. `/sync` will always return everything, with limit:
  10)
- joining a room after you have logged in won't give you the room events

## Elixir primer

You shouldn't need to know much about Elixir to write a simple test script.
(More complicated tests that need to implement custom server behaviours would
be a different story.)  Ideally, you should be able to copy an existing test
and modify it.

- Elixir uses a Ruby-like syntax, but is not the same as Ruby
  - comments start with `#` and continue to the end of the line
  - code blocks are delimited by words (usually `do ... end`, rather than by
    braces like in C-like languages, or using indentation like in Python)
- basic data types:
  - numbers
  - strings (e.g. `"foobar"` -- note: single quotes and double quotes are
    different.  You will almost always want to use double quotes) and binaries
    (e.g. `<<1, 2, 3>>`), which are the same thing, but the term "string" is
    usually used to describe something that is intended to be human-readable,
    while "binary" is usually used to describe something that is not,
  - lists (e.g. `[1, 2, 3]` -- usually every element is the same type),
  - tuples (e.g. `{1, "foo"}`),
  - atoms (e.g. `:foo` -- generally used for pre-defined values, such as error
    codes)
  - map (e.g. `%{"foo" => "bar"}` or `%{foo: "bar"}` if the key is an atom),
- data in Elixir is immutable, which means that you do not modify a data
  structure, but you create a new data structure (which may share parts of
  the old data structure) with the modifications that you want to make.  For
  example, you cannot do something like `map["key"] = value` to modify a map.
  Instead, you call `new_map = Map.put(map, "key", value)` (or `map =
  Map.put(map, "key", value)`), which will create a new map with the new value
  stored for the key `"key"`
- variable "assignment" is actually pattern matching, and can do destructuring.
  e.g. `Polyjuice.ClientTest.BaseTestServer.create_user()` returns a tuple, so
  you can do `{user, password} =
  Polyjuice.ClientTest.BaseTestServer.create_user()` to extract the returned
  user ID and password.
  - if you know some values that should be returned, you can include them in
    the pattern to ensure that it is what is returned.  For example, if you
    know what `password` will be, you can do `{user, "password"} =
    Polyjuice.ClientTest.BaseTestServer.create_user()`
  - if you don't care about a value, you can use prefix the variable name with
    `_`, or just use the variable name `_`.  e.g. if you don't care about the
    password, you can do `{user, _password} =
    Polyjuice.ClientTest.BaseTestServer.create_user()` or `{user, _} =
    Polyjuice.ClientTest.BaseTestServer.create_user()`.
  - `^` is the pin operator, and will only allow matches with a variable if the
    existing value of the variable matches the returned value.  e.g. `{user,
    ^password} = Polyjuice.ClientTest.BaseTestServer.create_user()` will only
    succeed if the new password is the same as the current value of
    `password`.  (Without the `^`, `password` will be overwritten with whatever
    value was returned.)
- error handling in Elixir is usually done by returning something like `{:ok,
  value}` on success, and `{:error, code}` on failure, rather than using
  exceptions or errors.
- anonymous functions are written as:
  ```elixir
  fn argument, list ->
    function body
  end
  ```
  The argument list can use pattern matching (using the same features as with
  variable assignment), and you can have multiple patterns in a function.
  Patterns will be tried in order.  For example,
  ```elixir
  fn
    1 -> "one"
    2 -> "two"
    _ -> "something else"
  end
  ```
  If none of the patterns match (e.g. if the last pattern was missing in the
  function above, and the function was called on a value other than 1 or 2), an
  error will be raised.  If this happens in a test script and the error is not
  handled, the test will fail with an internal error.
- doing something like
  ```elixir
  x = "foo"
  if condition do
    x = "bar"
  end
  ```
  will not do what you expect it to do.  It will create a variable `x` with
  value `"bar"` inside the `if` statement, but after the `if` statement is
  done, it will revert back to its previous value.  To conditionally change a
  variable, you would do something like
  ```elixir
  x = if condition do
        "bar"
      else
        x
      end
  ```
  This assigns the value of the `if` statement to `x`.  If `condition` is
  `true` (or truthy), the value of the `if` statement will be `"bar"`.
  Otherwise, the value of the `if` statement will be the old value of `x`.
  (This can also be shortened to one line: `x = if condition, do: "bar",
  else: x` -- note the extra commas and colons in this form.)
- The pipe operator `|>` takes the value of one expression and uses it as the
  first argument in a function call.  For example, `"foo" |> String.length()`
  is the same as `String.length("foo")`.  This is often used to chain function
  calls together.
- adding some `alias` statements will help avoid having to type long module
  names.  For example, using `alias Polyjuice.ClientTest.Util`, you can call
  any of the functions from the submodules as `Util.SubModule.function()`
  rather than `Polyjuice.ClientTest.Util.SubModule.function()`.  For writing
  tests, it's probably a good idea to do `alias
  Polyjuice.ClientTest.BaseTestServer`, `alias Polyjuice.Server.Protocols`, and
  `Polyjuice.ClientTest.Util`.
