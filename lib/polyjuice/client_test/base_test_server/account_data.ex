# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.AccountData do
  @behaviour Polyjuice.Server.Protocols.Helper.AccountData

  # map of user ID to room ID to event type to event contents
  @type account_data_map :: %{
          optional(String.t()) => %{
            (String.t() | nil) => %{String.t() => Polyjuice.Util.event_content()}
          }
        }

  # list of {stream ID, user ID, room ID or nil, event type, event contents}
  @type account_data_stream ::
          list(
            {pos_integer, String.t(), String.t() | nil, String.t(),
             Polyjuice.Util.event_content()}
          )

  @type required_state() :: %{
          # map of user ID to room ID to event type to event contents
          account_data: account_data_map(),
          # list of {stream ID, user ID, event type, event contents}
          account_data_stream: account_data_stream()
        }

  @type optional_state_pub_sub() :: %{
          subscribers: %{optional(any) => Polyjuice.Server.Protocols.Subscriber.t()}
        }

  @impl Polyjuice.Server.Protocols.Helper.AccountData
  def set({user_id, room_id, type, data}, from, state) do
    account_data_for_user = Map.get(state.account_data, user_id, %{})
    account_data_for_room = Map.get(account_data_for_user, room_id, %{})
    new_account_data_for_room = Map.put(account_data_for_room, type, data)
    new_account_data_for_user = Map.put(account_data_for_user, room_id, new_account_data_for_room)
    new_account_data = Map.put(state.account_data, user_id, new_account_data_for_user)

    {new_account_data_stream, stream_pos} =
      case state.account_data_stream do
        [{pos, _, _, _, _} | _] ->
          {[{pos + 1, user_id, room_id, type, data} | state.account_data_stream], pos + 1}

        [] ->
          {[{1, user_id, room_id, type, data}], 1}
      end

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:set_account_data, user_id, room_id, type, data},
        state
      )

    state = %{
      state
      | account_data: new_account_data,
        account_data_stream: new_account_data_stream
    }

    {:reply, :ok, _state} =
      if Map.has_key?(state, :subscribers) do
        data =
          if room_id == nil do
            {:account_data, stream_pos, %{"type" => type, "content" => data}}
          else
            {:room_account_data, stream_pos, room_id, %{"type" => type, "content" => data}}
          end

        Polyjuice.ClientTest.BaseTestServer.PubSub.publish({data, [user_id]}, from, state)
      else
        {:reply, :ok, state}
      end
  end

  @impl Polyjuice.Server.Protocols.Helper.AccountData
  def get({user_id, room_id, type}, _from, state) do
    data = state.account_data |> Map.get(user_id, %{}) |> Map.get(room_id, %{}) |> Map.get(type)

    if data == nil do
      {:reply,
       {:error,
        %Polyjuice.Server.Plug.MatrixError{
          message: "Not found",
          plug_status: 404,
          matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
        }}, state}
    else
      {:reply, {:ok, data}, state}
    end
  end
end
