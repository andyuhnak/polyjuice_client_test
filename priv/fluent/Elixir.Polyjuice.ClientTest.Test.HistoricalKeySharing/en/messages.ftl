same-session-used = The same Megolm session was used for both messages.
session-marked-as-not-shareable = Megolm session marked as not shareable, but should be.
session-marked-as-shareable = Megolm session marked as shareable, but should not be.
shared-unshareable-session = A session was shared that should not have been.
