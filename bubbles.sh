#!/bin/sh
set -e
inkscape --export-type=png --export-area-page bubbles.svg
pngquant bubbles.png -f -o priv/static/bubbles.png
rm bubbles.png
