# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.AccountData do
  @moduledoc """
  Utility functions for account data.
  """

  @doc """
  Disable analytics where possible by setting account data values.

  Analytics on a client test is not very useful, so disable it to reduce nagging.

  Currently supports Element.
  """
  @spec disable_analytics(
          Polyjuice.Server.Protocols.AccountData.t(),
          String.t()
        ) :: :ok
  def disable_analytics(server, user_id) do
    Polyjuice.Server.Protocols.AccountData.set(server, user_id, nil, "im.vector.analytics", %{
      "pseudonymousAnalyticsOptIn" => false
    })

    :ok
  end

  def disable_analytics(user) do
    Polyjuice.Server.Protocols.AccountData.set(
      user.server,
      user.id,
      nil,
      "im.vector.analytics",
      %{
        "pseudonymousAnalyticsOptIn" => false
      }
    )

    user
  end

  def set_dms(user, rooms) do
    Polyjuice.Server.Protocols.AccountData.set(
      user.server,
      user.id,
      nil,
      "m.direct",
      rooms
    )

    user
  end
end
